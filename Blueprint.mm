<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000034" CREATED="1328799308671" ID="ID_3652132628" MODIFIED="1328799836281" STYLE="NodeBasicGraphic" TEXT="&#x425;&#x438;&#x43c;&#x438;&#x44f;">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="ts_thumb_down"/>
<icon BUILTIN="ts_thumb_up"/>
<node COLOR="#000034" CREATED="1328799308671" ID="ID_8732887756" MODIFIED="1328799850281" POSITION="right" STYLE="NodeMasterGraphic" TEXT="&#x411;&#x430;&#x433;&#x438;">
<edge COLOR="#fd3445"/>
<icon BUILTIN="stop-sign"/>
<node COLOR="#000034" CREATED="1328799308671" ID="ID_5813345375" MODIFIED="1328799308671" STYLE="NodeOvalGraphic" TEXT="&#x41d;&#x435;&#x442; &#x43f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x43a;&#x438; &#x43f;&#x440;&#x430;&#x432; &#x434;&#x43e;&#x441;&#x442;&#x443;&#x43f;&#x430;">
<edge COLOR="#fd7134"/>
<icon BUILTIN="yes"/>
<icon BUILTIN="pencil"/>
<node COLOR="#000034" CREATED="1328799308671" ID="ID_0742382154" MODIFIED="1328799308671" STYLE="NodeTextGraphic" TEXT="&#x41d;&#x430; &#x432;&#x441;&#x435; INI">
<edge COLOR="#309eff"/>
<icon BUILTIN="calendar"/>
</node>
<node COLOR="#000034" CREATED="1328799308671" ID="ID_3261854588" MODIFIED="1328799308671" STYLE="NodeTextGraphic" TEXT="&#x41d;&#x430; &#x43f;&#x430;&#x43f;&#x43a;&#x443; docs">
<edge COLOR="#309eff"/>
<icon BUILTIN="gohome"/>
</node>
<node COLOR="#000034" CREATED="1328799308671" ID="ID_5401135083" MODIFIED="1328799308671" STYLE="NodeBasicGraphic" TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x438;&#x442;&#x44c; &#x43d;&#x430; &#x433;&#x43b;&#x430;&#x432;&#x43d;&#x443;&#x44e;">
<edge COLOR="#58fd34"/>
<icon BUILTIN="ts_android"/>
</node>
</node>
<node COLOR="#000034" CREATED="1328799308687" ID="ID_4564848828" MODIFIED="1328799308687" STYLE="NodeOvalGraphic" TEXT="&#x41d;&#x43e;&#x440;&#x43c;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x438;&#x43c;&#x44f; &#x434;&#x43b;&#x44f; &#x43f;&#x435;&#x447;&#x430;&#x442;&#x438;">
<edge COLOR="#fdc334"/>
<icon BUILTIN="yes"/>
<icon BUILTIN="pencil"/>
</node>
<node COLOR="#000034" CREATED="1328799308687" ID="ID_4177241236" MODIFIED="1328799308687" STYLE="NodeTextGraphic" TEXT="&#x41e;&#x431;&#x440;&#x430;&#x431;&#x43e;&#x442;&#x43a;&#x430; &#x41f;&#x435;&#x440;&#x435;&#x437;&#x430;&#x43f;&#x438;&#x441;&#x44c; &#x444;&#x430;&#x439;&#x43b;&#x43e;&#x432;">
<edge COLOR="#fd7134"/>
</node>
<node COLOR="#000034" CREATED="1328799308687" ID="ID_6001340118" MODIFIED="1328799839890" STYLE="NodeTextGraphic" TEXT="&#x41d;&#x43e;&#x440;&#x43c;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x439; &#x444;&#x43e;&#x43a;&#x443;&#x441;">
<edge COLOR="#fd7134"/>
<arrowlink DESTINATION="ID_6001340118" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_79936882" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
</node>
<node COLOR="#000034" CREATED="1328799308687" ID="ID_6226306472" MODIFIED="1328887769375" POSITION="left" STYLE="NodeMasterGraphic" TEXT="&#x414;&#x43e;&#x431;&#x430;&#x432;&#x438;&#x442;&#x44c;">
<edge COLOR="#55fd34"/>
<icon BUILTIN="bookmark"/>
<node COLOR="#000034" CREATED="1328799308687" ID="ID_0685688480" MODIFIED="1328799308687" STYLE="NodeOvalGraphic" TEXT="&#x41e;&#x448;&#x438;&#x431;&#x43a;&#x438; &#x43d;&#x430; EMail">
<edge COLOR="#fde034"/>
<icon BUILTIN="attach"/>
</node>
<node COLOR="#000034" CREATED="1328799308687" ID="ID_8370828311" MODIFIED="1328799308687" STYLE="NodeOvalGraphic" TEXT="&#x412;&#x435;&#x434;&#x435;&#x43d;&#x438;&#x435; &#x441;&#x432;&#x43e;&#x435;&#x433;&#x43e; &#x43b;&#x43e;&#x433;&#x430;">
<edge COLOR="#fdd734"/>
<icon BUILTIN="pencil"/>
</node>
<node COLOR="#000034" CREATED="1328799308687" ID="ID_0042381145" MODIFIED="1328799308687" STYLE="NodeOvalGraphic" TEXT="&#x422;&#x435;&#x43a;&#x441;&#x442; &#x43d;&#x430; &#x433;&#x43b;&#x430;&#x432;&#x43d;&#x43e;&#x439;">
<edge COLOR="#fdd734"/>
<icon BUILTIN="pencil"/>
</node>
<node CREATED="1328811992859" ID="ID_546391767" MODIFIED="1328812000890" TEXT="&#x41d;&#x43e;&#x440;&#x43c;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x435; &#x434;&#x43e;&#x431;&#x430;&#x432;&#x43b;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x430;&#x440;&#x43e;&#x43b;&#x435;&#x439;"/>
</node>
<node CREATED="1328799977046" ID="ID_726928383" MODIFIED="1328814995671" POSITION="right" TEXT="&#x423;&#x431;&#x440;&#x430;&#x43d;&#x43d;&#x44b;&#x435; &#x431;&#x430;&#x433;&#x438;">
<node COLOR="#000034" CREATED="1328799308671" HGAP="27" ID="ID_4164441018" MODIFIED="1328815130546" STYLE="NodeOvalGraphic" TEXT="XSS!!!" VSHIFT="12">
<edge COLOR="#fd7777"/>
<font BOLD="true" NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="clanbomber"/>
</node>
<node COLOR="#000034" CREATED="1328799308687" ID="ID_2480542361" MODIFIED="1328799308687" STYLE="NodeTextGraphic" TEXT="&#x41f;&#x440;&#x438; &#x432;&#x44b;&#x431;&#x43e;&#x440;&#x435; &#x43f;&#x443;&#x441;&#x442;&#x43e;&#x442;&#x44b; &#x431;&#x430;&#x433;">
<edge COLOR="#fd7134"/>
</node>
<node CREATED="1328799852015" ID="ID_1391418127" MODIFIED="1328799883593" TEXT="&#x423;&#x431;&#x440;&#x430;&#x442;&#x44c; &#x434;&#x432;&#x43e;&#x439;&#x43d;&#x43e;&#x439; &#x43b;&#x43e;&#x433;&#x438;&#x43d;">
<arrowlink DESTINATION="ID_1391418127" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_623342826" STARTARROW="None" STARTINCLINATION="0;0;"/>
<node CREATED="1328799867500" ID="ID_636714700" MODIFIED="1328799880625" TEXT="&#x417;&#x430;&#x431;&#x43b;&#x43e;&#x43a;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x442;&#x44c; &#x43a;&#x43d;&#x43e;&#x43f;&#x43a;&#x443; &quot;&#x412;&#x445;&#x43e;&#x434;&quot; &#x43d;&#x430; &#x432;&#x440;&#x435;&#x43c;&#x44f; &#x43f;&#x440;&#x43e;&#x432;&#x435;&#x440;&#x43a;&#x438;"/>
<node CREATED="1328799885265" ID="ID_1798058082" MODIFIED="1328799894140" TEXT="&#x417;&#x430;&#x431;&#x43b;&#x43e;&#x43a;&#x438;&#x440;&#x43e;&#x432;&#x430;&#x442;&#x44c; &#x43a;&#x43d;&#x43e;&#x43f;&#x43a;&#x443; &#x43f;&#x43e;&#x441;&#x43b;&#x435; &#x443;&#x441;&#x43f;&#x435;&#x448;&#x43d;&#x43e;&#x433;&#x43e; &#x432;&#x445;&#x43e;&#x434;&#x430;"/>
<node CREATED="1328799896500" ID="ID_857815410" MODIFIED="1328799901312" TEXT="&#x423;&#x43c;&#x435;&#x43d;&#x44c;&#x448;&#x438;&#x442;&#x44c; &#x440;&#x435;&#x434;&#x438;&#x440;&#x435;&#x43a;&#x442;"/>
</node>
<node COLOR="#000034" CREATED="1328799308687" ID="ID_2157187548" MODIFIED="1328799308687" STYLE="NodeTextGraphic" TEXT="&#x41b;&#x43e;&#x433;&#x438;&#x43d; &#x431;&#x435;&#x437; &#x440;&#x435;&#x433;&#x438;&#x441;&#x442;&#x440;&#x430;">
<edge COLOR="#fd7134"/>
</node>
</node>
<node CREATED="1328800039546" ID="ID_489902695" MODIFIED="1328800045343" POSITION="left" TEXT="&#x412;&#x432;&#x435;&#x434;&#x451;&#x43d;&#x43d;&#x44b;&#x435; &#x444;&#x438;&#x447;&#x438;">
<node COLOR="#000034" CREATED="1328799308687" ID="ID_5566877870" MODIFIED="1328799308687" STYLE="NodeMasterGraphic" TEXT="&#x41c;&#x435;&#x441;&#x442;&#x43e; - &#x423;&#x447;&#x430;&#x441;&#x442;&#x43d;&#x438;&#x43a;">
<edge COLOR="#fd344f"/>
</node>
<node CREATED="1328799968781" ID="ID_1610103750" MODIFIED="1328873460984" TEXT="&#x412;&#x44b;&#x445;&#x43e;&#x434; &#x431;&#x435;&#x437; &#x43d;&#x43e;&#x442;&#x438;&#x444;&#x438;&#x43a;&#x430;&#x446;&#x438;&#x438;"/>
</node>
</node>
</map>
