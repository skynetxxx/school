<?
$need[] = "INI";
$need[] = "CONFIG";
$need[] = "MYSQL";
include("config.php");

// �������� ���� ����������
if (!isset($_REQUEST['list'],$_REQUEST['data'],$_REQUEST['section'],$_SESSION['uid'])) ReturnError();

$data = $_REQUEST['data'];
$section_id = $_REQUEST['section'];
$user_id = $_SESSION['uid']; //�������� ID ������������ �� ���� ������
$list = $_REQUEST['list'];

PDO_Connect(); //����������� � �����

if (!SectionCanWrite($user_id,$section_id)) ReturnError();

ReplaceSectionValues($section_id,$data);
ReturnSuccess();

function ReturnError() {	Header("HTTP/1.1 400 Bad Request"); //���������� ��������� ������
	die(json_encode(array("status"=>"error","code"=>"400","message"=>"Bad Request"))); //������ � JSON
}

function ReturnSuccess() {	header("HTTP/1.1 200 OK");
	die(json_encode(array("status"=>"success","code"=>"200","message"=>"ok"))); //������ � JSON
}
?>