<?
include_once("config.php");
$DBH = false; //�� ���������
PDO_Connect();

function PDO_Connect($onError=0) {global $DBH;
    if ($DBH!==false) return false; //���� ��� ������������ �����������
	try {
		@$DBH = new PDO("mysql:host=".MYSQL_HOST.";dbname=".MYSQL_BASE,MYSQL_USER,MYSQL_PASSWORD);
		$DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$DBH->exec("SET NAMES 'cp1251'");
	}
	catch (PDOException $e) {
		$error = $e->GetMessage(); //��������� �� ������
		$DBH = false; //������� �������
		if ($onError!==0) { $onError($error); } //��������� ���������� ������� ������
		else { echo defined("MYSQL_TEXT_ERROR") ? MYSQL_TEXT_ERROR : "<h3 color='#f00'>$error</h3>"; } //���� ����� ������������ ����� ������, �� ���������� ���. ����� ������ ������� ������
		if (defined("MYSQL_ERROR_DIE") AND (MYSQL_ERROR_DIE===true)) die(); //���� ����������� ����� ��� ������ ���������� � �����
	}
}

function CheckLogin($login) {
	global $DBH; //��������� ������
	$STH = $DBH->prepare("SELECT * FROM `users` WHERE `login`=? LIMIT 1"); //�������� �� ���� �������
	$STH->bindParam(1,$login); //��������� �����
    $STH->execute(); //��������� SQL ������
    return ($STH->rowCount()===1) ? true : false; //���������� ������, ���� ����� ����������. ����� - false
}

function TryLogin($login,$pass) {
	global $DBH; //��������� ������
	$STH = $DBH->prepare("SELECT * FROM `users` WHERE `login`=? AND `pass`=? LIMIT 1"); //�������� �� ���� �������
	$STH->bindParam(1,$login); //��������� �����
	$STH->bindParam(2,$pass); //� ������
    $STH->execute(); //��������� SQL ������
    if ($STH->rowCount()===0) return false; //���� ������ ������������ ���
    return ($STH->fetch()); //���� ����
}

// ���������� ���� ������ �� �����
function GetSectionName($section_id,$list_id,$user_id) {	global $DBH; //��������� ������
	$STH = $DBH->prepare("SELECT `value` FROM `lists` INNER JOIN `section` USING(`lid`) WHERE `uid`=:uid AND `lid`=:lid AND `sid`=:sid"); //�������� �� ���� �������
    $STH->execute(array("sid"=>$section_id,"lid"=>$list_id,"uid"=>$user_id)); //��������� SQL ������
	$result = $STH->fetch();
	return $result;
}

function GetSectionById($section_id) {
	global $DBH; //��������� ������
	$STH= $DBH->prepare("SELECT `id`,`section_values`.`value`,`template` FROM `section` INNER JOIN `section_values` USING(`sid`) WHERE `sid`=:sid ORDER BY `id`");
	$STH->execute(array("sid"=>$section_id));
	$result = $STH->fetchAll();
	return $result;
}

// ************************************************************************
// ********************    JSON.PHP ***************************************
// ************************************************************************

function GetSectionsIdByList($lid) {	global $DBH; //��������� ������
	$STH= $DBH->prepare("SELECT `sid` FROM `section` WHERE `lid`=:lid");
	$STH->execute(array("lid"=>$lid));
	$result = $STH->fetchAll();
	$new_result = array();
	foreach ($result as $r) {		$new_result[] = $r["sid"];	}
	return $new_result;
}



function ReplaceListValues($lid,$data,$count) {	global $DBH; //��������� ������
	$STH = $DBH->prepare("DELETE FROM `lists_values` WHERE `lid`=:lid"); //������� ������� ��������
	$STH->bindParam("lid",$lid); //��������� �������
    $STH->execute(); // ��������� ������� ��������

    $STH = $DBH->prepare("UPDATE `lists` SET `count`=:count WHERE `lid`=:lid"); //������������� ����� ��������� � ��������� �����
    $STH->execute(array("lid"=>$lid,"count"=>$count));
    $STH->execute();

    $STH = $DBH->prepare("INSERT INTO `lists_values` (`lid`,`nid`,`sid`,`value`) VALUES(:lid,:index,:section_id,:value)\n");

	foreach ($data as $section_id=>$values) {		foreach ($values as $index=>$val) {			$bindParam = array("lid"=>$lid,"index"=>$index,"section_id"=>$section_id,"value"=>$val);
			$STH->execute($bindParam);
			$STH->closeCursor();		}	}
}

function OldReplaceListValues($lid,$data,$count) {
	global $DBH; //��������� ������
	$DBH->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true); //��������� �����������
	$STH = $DBH->prepare("DELETE FROM `lists_values` WHERE `lid`=:lid"); //������� ������� ��������
	$STH->bindParam("lid",$lid); //��������� �������
    $STH->execute(); // ��������� ������� ��������

    $STH = $DBH->prepare("UPDATE `lists` SET `count`=:count WHERE `lid`=:lid");
    $STH->execute(array("lid"=>$lid,"count"=>$count));
    $STH->execute();

    $STH = $DBH->prepare('
        SET @LID:=:lid;
    	# �������� ������ ������ �� ������ �� ������ �������
		SELECT @VALUE:=`value` FROM `section` WHERE `value`="������� ��� ��������" AND `lid`=@LID;
		SELECT @SID1:=`sid` FROM `section` WHERE `value`=@VALUE AND `lid`=@LID;
		SELECT @VALUE:=`value` FROM `section` WHERE `value`="���� ������" AND `lid`=@LID;
		SELECT @SID2:=`sid` FROM `section` WHERE `value`=@VALUE AND `lid`=@LID;
		SELECT @VALUE:=`value` FROM `section` WHERE `value`="�����" AND `lid`=@LID;
		SELECT @SID3:=`sid` FROM `section` WHERE `value`=@VALUE AND `lid`=@LID;
		SELECT @VALUE:=`value` FROM `section` WHERE `value`="���������" AND `lid`=@LID;
		SELECT @SID4:=`sid` FROM `section` WHERE `value`=@VALUE AND `lid`=@LID;
		SELECT @VALUE:=`value` FROM `section` WHERE `value`="�����" AND `lid`=@LID;
		SELECT @SID5:=`sid` FROM `section` WHERE `value`=@VALUE AND `lid`=@LID;
		SELECT @VALUE:=`value` FROM `section` WHERE `value`="������" AND `lid`=@LID;
		SELECT @SID6:=`sid` FROM `section` WHERE `value`=@VALUE AND `lid`=@LID;
    ');
    $STH->bindParam("lid",$lid);
    $STH->execute();

    $STH = $DBH->prepare('
		# ���������� (����� ��� �������� ��������� � �������)
		SET @LID:=:lid;
		SET @NID:=:nid;

		# ��������������� ������� ������ � �������
		INSERT INTO `lists_values` (`lid`,`nid`,`sid`,`value`) VALUES
		(@LID,@NID,@SID1,:fio),
		(@LID,@NID,@SID2,:tem),
		(@LID,@NID,@SID3,:kls),
		(@LID,@NID,@SID4,:nom),
		(@LID,@NID,@SID5,:mes),
		(@LID,@NID,@SID6,:sec); #SQL END
    ');
    $n=1;
    foreach ($data as $row) {
    	$bindParam = array("lid"=>$lid,
    						"nid"=>$n,
					    	"fio"=>$row[0],
					    	"tem"=>$row[1],
					    	"kls"=>$row[2],
					    	"nom"=>$row[3],
					    	"mes"=>$row[4],
					        "sec"=>$row[5]
        					);
        $n++;
        $STH->execute($bindParam);
        $STH->closeCursor();
    }
}



// ************************************************************************





// ***********************************************************************
// ********************    MAIN.PHP **************************************
// ************************************************************************
function GetAllListValues($list_id) {
	global $DBH; //��������� ������
	$STH = $DBH->prepare("SELECT `nid`,`sid`,`value` FROM `lists_values` WHERE `lid`=? ORDER BY `nid`"); //�������� �� ���� �������
	$STH->bindParam(1,$list_id);
    $STH->execute(); //��������� SQL ������
	$result = $STH->fetchAll();
	$STH->closeCursor();
	$new_result = array();
	foreach ($result as $r) {
		$new_result[$r["sid"]][$r["nid"]] = $r["value"];
	}
	return $new_result;
}

function GetListInfo($list_id) {
	global $DBH; //��������� ������
	$STH = $DBH->prepare("SELECT * FROM `lists` WHERE `lid`=?"); //�������� �� ���� �������
	$STH->bindParam(1,$list_id);
    $STH->execute(); //��������� SQL ������
	$result = $STH->fetch();
	$STH->closeCursor();
	return $result;
}
function GetSectionsValues($list) {	global $DBH; //��������� ������
	$STH= $DBH->prepare("SELECT `sid`,`id`,`section_values`.`value` FROM `section_values` INNER JOIN `section` USING(`sid`) WHERE `lid`=:lid");
	$STH->bindParam("lid",$list);
	$STH->execute();
	$result = $STH->fetchAll();
	$new_result = array();
	foreach ($result as $v) {
		$new_result[$v["sid"]][$v["id"]] = $v["value"];
	}
    return $new_result;
}

function GetSections($list) {	global $DBH; //��������� ������
	$STH= $DBH->prepare("SELECT `sid`,`template`,`value`,`selector` FROM `section` WHERE `lid`=:lid");
	$STH->bindParam("lid",$list);
	$STH->execute();
	$result = $STH->fetchAll();
	$new_result = array();
	foreach ($result as $s) {
		$new_result[$s["sid"]] = array("sid"=>$s["sid"],"name"=>$s["value"],"template"=>$s["template"],"selector"=>$s["selector"]);;
	}
	return $new_result;
}

function GetFullSections($list_id) {
$sections = GetSections($list_id);
$section_values = GetSectionsValues($list_id);
$list_values = GetAllListValues($list_id);
foreach ($sections as $sid=>$section) {
	//���� ���������� �������� ��� ������ �� ������
	if (array_key_exists($sid,$section_values)) { $sections[$sid]["values"] = $section_values[$sid]; } 	    //��������� �� � ������� ������
    if (array_key_exists($sid,$list_values))    { $sections[$sid]["table"]  =    $list_values[$sid]; }
}
unset($section_values,$list_values); //������� ������
return $sections;
}

// ***************************************************************************



function GetSectionSelectors($list,$find_text=false) {	global $DBH; //��������� ������
	if ($find_text===true) { //���� ����� �������� ������ �����
		$STH= $DBH->prepare("SELECT `sid`,`template`,`value` FROM `section` WHERE `selector`=0 AND `lid`=:lid");
	} elseif ($find_text==="all") { //���� �������� �� (� ���������, � �����)		$STH= $DBH->prepare("SELECT `sid`,`template`,`value`,`selector` FROM `section` WHERE `lid`=:lid");
	} else { //���� ������ ���������		$STH= $DBH->prepare("SELECT `sid`,`template`,`value` FROM `section` WHERE `selector`=1 AND `lid`=:lid");
	}

	$STH->bindParam("lid",$list);
	$STH->execute();
	$result = $STH->fetchAll();
	return $result;
}

// ����� �� ������������ ������ � ��� ������
function SectionCanWrite($user_id,$section_id) {	global $DBH; //��������� ������
	$STH = $DBH->prepare("SELECT `uid`,`sid` FROM `lists` INNER JOIN `section` USING(`lid`) WHERE `uid`=:uid AND `sid`=:sid");
	$data = array("uid"=>$user_id,"sid"=>$section_id);
	$STH->execute($data);
	return ($STH->rowCount()==1) ? true : false;
}

// ���������� ��������� ������ �� �����
function GetSection($section_id) {
	global $DBH; //��������� ������
	$STH = $DBH->prepare("SELECT * FROM `section` WHERE `id`=?"); //�������� �� ���� �������
	$STH->bindParam(1,$section_id);
    $STH->execute(); //��������� SQL ������
	$result = $STH->fetch();
	return $result;
}

// �������� ���������� ������ �� � ID
function GetSectionValues($sid) {	global $DBH; //��������� ������	$STH = $DBH->prepare("SELECT `value` FROM `section_values` WHERE `sid`=? ORDER BY `id`"); //�������� ������ �� ������� ������
	$STH->bindParam(1,$sid);
    $STH->execute(); //��������� SQL ������
	$result =  $STH->fetchAll();
	return $result;
}


function ReplaceSectionValues($sid,$data) {	global $DBH; //��������� ������
	$STH = $DBH->prepare("DELETE FROM `section_values` WHERE `sid`=:sid"); //������� ������� ��������
	$STH->bindParam("sid",$sid); //��������� �������
    $STH->execute(); // ��������� ������� ��������

    $STH = $DBH->prepare("INSERT INTO `section_values` (`sid`,`id`,`value`) VALUES(:sid,:id,:value)"); //������� ����� ������
    foreach ($data as $cid => $value) { //������� ��� �������� �������    	$text = iconv("utf-8","cp1251",$value); //� ���������� ���������
    	$text = htmlspecialchars($text);
    	$binder = array("sid"=>$sid,"id"=>$cid,"value"=>$text); //��������� ��� SQL �������
    	$STH->execute($binder); //��������� SQL ������
    }
}

function GetListTemplates($list_id) {	global $DBH; //��������� ������
	$STH = $DBH->prepare("SET @LID:=:lid;");
    $STH->bindParam("lid",$list_id);
    $STH->execute();

    $STH = $DBH->prepare('
    	# �������� ������ ������ �� ������ �� ������ �������
		SELECT `template` FROM `section` WHERE `value`="������� ��� ��������"  AND `lid`=@LID
		OR `value`="���� ������"           AND `lid`=@LID
		OR `value`="�����"                 AND `lid`=@LID
		OR `value`="���������"             AND `lid`=@LID
		OR `value`="�����"                 AND `lid`=@LID
		OR `value`="������"                AND `lid`=@LID
    ');
    $STH->execute();
    return $STH->fetchAll();
}

function GetSectionByValue($lid,$value,$only_selectors=false) {	global $DBH; //��������� ������
	if ($only_selectors===true) {		$STH = $DBH->prepare("SELECT `sid` FROM `section` WHERE `selector`=1 AND `lid`=:lid AND `value`=:value LIMIT 1"); //�������� ������ �� ������� ������
	} else {
		$STH = $DBH->prepare("SELECT `sid` FROM `section` WHERE `lid`=:lid AND `value`=:value LIMIT 1"); //�������� ������ �� ������� ������
	}
	$STH->bindParam("lid",$lid);
	$STH->bindParam("value",$value);
    $STH->execute(); //��������� SQL ������
	return $STH->fetch();}

function GetAllLists($uid) {	global $DBH; //��������� ������
	$STH = $DBH->prepare("SELECT * FROM `lists` WHERE `uid`=? ORDER BY `lid`"); //�������� ������ �� ������� ������
	$STH->bindParam(1,$uid);
    $STH->execute(); //��������� SQL ������
	return $STH->fetchAll();}


// ���������� ��������� ������ �� �����
function GetListUser($list_id) {
	global $DBH; //��������� ������
	$STH = $DBH->prepare("SELECT `uid` FROM `lists` WHERE `lid`=?"); //�������� �� ���� �������
	$STH->bindParam(1,$list_id);
    $STH->execute(); //��������� SQL ������
	$result = $STH->fetch();
	return $result["uid"];
	$STH->closeCursor();
}

function RenameList($lid,$new_name) {	global $DBH; //��������� ������
	$new_name = htmlspecialchars($new_name); //������ �� XSS
	$STH = $DBH->prepare("UPDATE `lists` SET `name`=:name WHERE `lid`=:lid");
	$STH->bindParam("lid",$lid);
	$STH->bindParam("name",$new_name);
	$STH->execute();
}

function DeleteList($lid) {	global $DBH; //��������� ������
    $bindParam = array("lid"=>$lid);
    // ������� �������� ������
	$STH = $DBH->prepare("DELETE FROM `lists` WHERE `lid`=:lid");
	$STH->execute($bindParam);
	// ������� ������ ������
	$STH = $DBH->prepare("DELETE FROM `lists_values` WHERE `lid`=:lid");
	$STH->execute($bindParam);
	// ������� ����
	$STH = $DBH->prepare("DELETE FROM `section` WHERE `lid`=:lid");
	$STH->execute($bindParam);
	// ������� �������� ������
	//$STH = $DBH->prepare("DELETE FROM `section_values` WHERE `lid`=:lid");
	//$STH->execute($bindParam);
}

function GetListValues($list_id) {	global $DBH; //��������� ������
	//$new_name = htmlspecialchars($new_name); //������ �� XSS
	$STH = $DBH->prepare("SELECT `nid`,`value` FROM `lists_values` WHERE `lid`=:lid ORDER BY `nid`,`sid`");
	$STH->bindParam("lid",$list_id);
	$STH->execute();
	return $STH->fetchAll();}


// ��������� ����� ������
function AddSection($lid,$value,$template,$selector,$default_value) {	global $DBH;
	if ($selector===true) { $selector =1; } else { $selector=0; }
	$STH = $DBH->prepare("SELECT `count` FROM `lists` WHERE `lid`=:lid");
	$STH->execute(array("lid"=>$lid));
	$count = $STH->fetch(); $count = $count["count"]; //�������� ����� ��������

	$STH = $DBH->prepare("INSERT INTO `section` (`lid`,`value`,`template`,`selector`) VALUES (:lid,:val,:temp,:selector)");
	$param = array("lid"=>$lid,"val"=>$value,"temp"=>$template,"selector"=>$selector);
	$STH->execute($param);
	$last = $DBH->lastInsertId(); //��������� ����������� ������
	$STH->closeCursor();

	// �������� � ����� ��������� ��������
    $STH = $DBH->prepare("INSERT INTO `lists_values` (`lid`,`nid`,`sid`,`value`) VALUES (:lid,:nid,:sid,:value)");
	foreach (range(1,$count,1) as $i) {
      $param = array("lid"=>$lid, "nid"=>$i, "sid"=>$last, "value"=>$default_value);
      $STH->execute($param);
      $STH->closeCursor();	}}

// �������� ������ �� �����
function ChangeTemplate($lid,$value,$template) {	global $DBH;
	$STH = $DBH->prepare("UPDATE `section` SET `template`=:template WHERE `lid`=:lid AND `value`=:value");
	$param = array("lid"=>$lid,"value"=>$value,"template"=>$template);
	$STH->execute($param);
	$STH->closeCursor();}

function RenameSection($lid,$old_name,$new_name) {	global $DBH;
	$STH = $DBH->prepare("UPDATE `section` SET `value`=:new_value WHERE `lid`=:lid AND `value`=:old_value");
	$param = array("lid"=>$lid,"old_value"=>$old_name,"new_value"=>$new_name);
	$STH->execute($param);
	$STH->closeCursor();}


function IsSelector($lid,$sid) {	global $DBH;
	$STH = $DBH->prepare("SELECT `selector` FROM `section` WHERE `lid`=:lid AND `sid`=:sid");
	$STH->bindParam("lid",$lid);
	$STH->bindParam("sid",$sid);
	$STH->execute();
	$data = $STH->fetch();
	$STH->closeCursor();
	return ($data["selector"]==="1") ? true : false;}

// ������ ��� ���� � ���� ������
function ChangeSectionType($lid,$value) {	global $DBH;
	ClearSectionValues($lid,$value); //������� ��� ������ �������� �� ������� ��������, ���� ��� ����
	$STH = $DBH->prepare('UPDATE `section` SET `selector`=1-`selector` WHERE `lid`=:lid AND `value`=:value');
	$STH->bindParam("lid",$lid);
	$STH->bindParam("value",$value);
	$STH->execute();
	$STH->closeCursor();}


// ������� ������ �� �������� � �����
function DeleteSectionByValue($lid,$value) {	global $DBH;
	ClearSectionValues($lid,$value); //������� ��� �������� ����� ����
	$STH = $DBH->prepare('DELETE FROM `section` WHERE `lid`=:lid AND `value`=:value;');
	$STH->bindParam("lid",$lid);
	$STH->bindParam("value",$value);
	$STH->execute();
	$STH->closeCursor();
}

function ClearSectionValues($lid,$value) {	global $DBH;
	$STH = $DBH->prepare('
	SELECT @SID:=`sid` FROM `section` WHERE `lid`=:lid AND `value`=:value;
	DELETE FROM `section_values` WHERE `sid`=@SID;
	');
	$STH->bindParam("lid",$lid);
	$STH->bindParam("value",$value);
	$STH->execute();
	$STH->closeCursor();}


function CreateEmptyList($user_id,$new_name) {global $DBH;
$STH = $DBH->prepare("INSERT INTO `lists` (`name`,`uid`,`count`) VALUES (:name,:uid,0)");
$STH->bindParam("name",$new_name);
$STH->bindParam("uid",$user_id);
$STH->execute();
$STH->closeCursor();}

function CopyFirstList($user_id,$new_name) {global $DBH;$STH = $DBH->prepare("INSERT INTO `lists` (`name`,`uid`,`count`) VALUES (:name,:uid,0)");
$STH->bindParam("name",$new_name);
$STH->bindParam("uid",$user_id);
$STH->execute();
$LID = $DBH->lastInsertId();  //�������� ��������� ���� ����������
$STH->closeCursor();


$STH = $DBH->prepare("
TRUNCATE TABLE `section_temp`;# ������� ��������� ������� (�� ������ ������)
INSERT INTO `section_temp` # ��������� �� ��������� ������
SELECT * FROM `section` WHERE `lid`=1; # �������� �� �� ����� ����� 1
UPDATE `section_temp` SET `lid`=:lid; # ������������� ����� �������� �� ��������� �������
INSERT INTO `section` (`value`,`template`,`selector`,`lid`)
SELECT `value`,`template`,`selector`,`lid` FROM `section_temp`;
TRUNCATE TABLE `section_temp`; # ������� ��������� �������
");
$STH->bindParam("lid",$LID);
$STH->execute();
$STH->closeCursor();
}

?>
