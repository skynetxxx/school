<?
/*# ���������� (����� ��� �������� ��������� � �������)
SET @LID:=1;
SET @NID:=4;
SET @FIO:="������� ��� ��������";
SET @TEM:="���� ������";
SET @KLS:="�����";
SET @NOM:="���������";
SET @MES:="�����";
SET @SEC:="������";

# �������� ������ ������ �� ������ �� ������ �������
SELECT @VALUE:=`value` FROM `section` WHERE `value`="������� ��� ��������";
SELECT @SID1:=`sid` FROM `section` WHERE `value`=@VALUE;
SELECT @VALUE:=`value` FROM `section` WHERE `value`="���� ������";
SELECT @SID2:=`sid` FROM `section` WHERE `value`=@VALUE;
SELECT @VALUE:=`value` FROM `section` WHERE `value`="�����";
SELECT @SID3:=`sid` FROM `section` WHERE `value`=@VALUE;
SELECT @VALUE:=`value` FROM `section` WHERE `value`="���������";
SELECT @SID4:=`sid` FROM `section` WHERE `value`=@VALUE;
SELECT @VALUE:=`value` FROM `section` WHERE `value`="�����";
SELECT @SID5:=`sid` FROM `section` WHERE `value`=@VALUE;
SELECT @VALUE:=`value` FROM `section` WHERE `value`="������";
SELECT @SID6:=`sid` FROM `section` WHERE `value`=@VALUE;

# ��������������� ������� ������ � �������
INSERT INTO `lists_values` (`lid`,`nid`,`sid`,`value`) VALUES
(@LID,@NID,@SID1,@FIO),
(@LID,@NID,@SID2,@TEM),
(@LID,@NID,@SID3,@KLS),
(@LID,@NID,@SID4,@NOM),
(@LID,@NID,@SID5,@MES),
(@LID,@NID,@SID6,@SEC); #SQL END*/

$need[] = "CONFIG";
$need[] = "MYSQL";
include("config.php");
$ERRORS = array();

if (!isset($_REQUEST['list'])) {	Header("Location: /");
	Die("<script>document.location = '/'</script>");
}

$list_id = $_REQUEST['list'];
$user_id = $_SESSION['uid'];

$list = GetListInfo($list_id);
$list_uid = $list["uid"];
$count = $list["count"];

if ($list_uid!==$user_id) die("��� �� ��� ����!");


$sections = GetFullSections($list_id);

$array = array();
foreach ($sections as $sec) { //���������� ����������� ������ � ����������
    $id = $sec["sid"]; //�������� ������ ��������
    if (!array_key_exists("table",$sec) OR !is_array($sec["table"])) break; //���� ��� �� ������ ��� ����� �� ����������, �� ������� ������
    foreach ($sec["table"] as $nid=>$val) {//������� ��� ������ �������� ��� ������ ������
    	$array[$nid][$id] = $val; // ������[����� ��������][����� ������] = ��������;    }
}
$table_rowspan = count($sections) +1; //�������� ������ ���� �����


function CreateInputs() {
global $array;foreach ($array as $a) {CreateNewInput($a);}
// ���������� ��������� �� ���������� ��������
if (count($array) == 0) NoContent();
}


function NoContent() {
?>
<tr id="nocontent1">
<td colspan="8">
<div style="margin-left: 100px; margin-bottom: 15px;">
<span class="label warning">��������</span> ��� ��������� ��� �����������.
</div>
</td>
</tr>

<?
}

function CreateNewInput($array) {
?>
 <tr>
  <td class="sid"><input type="checkbox" class="chk"></td>
  <?
  unset($array[0]); //������� ������� � �������
  foreach($array as $n=>$value) {?>
  <td class="td<?=$n;?>"><?=$value;?></td>
  <?}?>
 </tr>
<?
}

function FileList() {
global $files;
$array = array(); // ������ ������
if ((is_array($files)) AND (count($files)>0)) {
  foreach ($files as $f) { $array[] = "\"$f\""; } // ��������� � �������
  $ctext = implode(",",$array); // ��������� ������
  $ctext = "[$ctext];"; // ��������� ������ �������
  echo $ctext;} else {  echo "[];";}}



function GetJsonSections() {	global $sections;
	$json = array();
	foreach ($sections as $s) { $json[] = (int) $s["sid"]; }
	echo json_encode($json);}

// ��������� ������� ������ ���������� (��� jQuery)
function GetSelectors($prefix) {	global $sections;
	$selectors = array();
	foreach ($sections as $s) {
		if ($s["selector"]==1) {
			if (isset($s["values"]) AND count($s["values"])>0) { //���� ������ �� ���������� �� ���������� ��� ������
	            $text = '["'.implode($s["values"],'", "').'"]'; // ["�������", "������� 2"]
    	        $selectors[$s["sid"]] = $text; //��������� ������ � ������ � ������ ��������� � ���������
            } else {            	$selectors[$s["sid"]] = "[]";            }
 		}
 	}
    foreach ($selectors as $n =>$id) {    	echo "\"$prefix$n\": $id,\n";    }}


function GetSecType($v,$prefix="") {	global $sections; //���������� ���������� ������
	$arr_result = array();	foreach ($sections as $n=>$s) {		if ($s["selector"]==$v) {			$arr_result[] = $prefix.$n;		}	}
	$result = implode($arr_result,", ");
	return "\"$result\"";}
?>
<!DOCTYPE html>
<html>
<head>
<title>�������������� ������� ���������� ����������</title>
<link rel="stylesheet" href="/core/bootstrap.min.css">
<link rel="stylesheet" href="/core/table.css">
<script type="text/javascript" src="/core/jquery.js"></script>
<script type="text/javascript" src="/core/jquery.form.js"></script>
<script type="text/javascript" src="/core/bootstrap-alerts.js"></script>
<script type="text/javascript" src="/core/jquery.json-2.3.min.js"></script>
<script type="text/javascript" src="/core/jquery.isjson.js"></script>
<script type="text/javascript" src="/core/jquery.color.js"></script>
<script type="text/javascript" src="/core/table.js"></script>
<script type="text/javascript">
window.ledit = false;
window.selectors = { edit: <?=GetSecType(0,".td");?>,
 select: <?=GetSecType(1,".td");?>};
window.components = {<?=GetSelectors("td");?>}
window.saveindex = <?=GetJsonSections();?>

window.printid = {};
window.print = {};
window.print.range = "";
window.print.files = <?=FileList()."\n";?>
window.checkboxes = {};
window.list_id = <?=$list_id;?>;

var IE='\v'=='v'; // IE �������

$(document).ready(function(){
if ($("#nocontent1").length) {
	$(".btn_add").click(function(){
		 $("#nocontent1").fadeOut();;
		});
}

SetDblClick();
Button.Print.Create(window.print.files);
});

$.ajaxSetup({cache:false});

function SetDblClick() {$(window.selectors.edit).dblclick(Control.Text.Edit).focusout(Control.Text.LostFocus);
$(window.selectors.select).dblclick(Control.Select.Edit).focusout(Control.Select.LostFocus);
$("input:checkbox.chk").click(Control.Checkbox.Select);
}
</script>
</head>
<body>
<!-- ����� �� �������� ���������� ������-->
<div class="alert-message info" id="alert1" style="display: none">
 <a class="close" href="#">�</a>
 <p>����� ����� ���� <strong>�������</strong> ��������</p>
</div>
<!-- ����� �� �������� ����������-->
<div class="alert-message success" id="alert2" style="display: none">
 <a class="close" href="#">�</a>
 <p>���� <strong>�������</strong> ���������</p>
</div>
<!-- ����� �� �������� ����������-->
<div class="alert-message error" id="alert3" style="display: none">
 <a class="close" href="#">�</a>
 <p>��������� ������ ��� ���������� ������ !</p>
 <p>����������, ����������� ��� ���.</p>
 <p>���� ��� ������ ����������, ��������� � ������������� �� ������ <a href="mailto:artyomskynet@gmail.com">artyomskynet@gmail.com</a></p>
</div>
<?
foreach ($ERRORS as $n=>$error) {
?>
<div class="alert-message error" id="alert<?=$i;?>">
 <a class="close" href="#" onclick="$(this).parents('div').fadeOut('slow'); return false">�</a>
 <p><?=$error;?></p>
</div>
<?
}
?>
<!-- ������������ � �������� ������ ���� -->
<div class="alert-message warning" id="confirm1" style="display: none; position: fixed; width: 100%; z-index: 3">
 <span>�� �������, ��� ������ ������� ��������� (<b id="confirm1_num">x</b>) ������� ���� ?</span>
 <div class="alert-actions">
  <a class="btn big btn_cancel" href="#">������</a>
  <a class="btn small btn_delete" href="#">�������</a>
 </div>
</div>
<a class="btn_back alert-message warning" href="/"></a>
<br/><br/>

<!-- ��������� ������� ��� ����������� -->
<table style="display: none">
 <tr id="table_tocopy">
  <td class="sid"><input type="checkbox" class="chk"></td>
  <?foreach ($sections as $i) {?>
  <td class="td<?=$i["sid"];?>">...</td>
  <?}?>
 </tr>
 <tr id="table_hr">
  <td colspan="8">
   <hr style="margin: 0px;">
  </td>
 </tr>
</table>
<div style="display: none">
 <input type="text" id="input_edit" onkeypress="">
</div>

<table class="bordered-table zebra-striped" style="margin-left: 30px; width: 90%" id="main_table">
 <tr id="table_title">
  <td>
    <div id="header">
      <a href="#footer"><img src="/images/down.png" title="������� � ����� ��������"></a>
    </div>
  </td>
  <th colspan="<?=$table_rowspan;?>" style="vertical-align: middle">
    <span class="title"><?=$list["name"];?></span>
	<a class="btn btn_add btn-add" href="#" onclick="Button.AddNew(); return false" style="float: right">�������� </a>
  </th>
 </tr>
 <tr id="table_header">
  <th><input type="checkbox" id="checkbox_main" onclick="Control.Checkbox.SelectAll(this);"></th>
  <? foreach($sections as $s) { ?>
  <th><?=$s["name"];?></th>
  <? } ?>
 </tr>
 <?=CreateInputs();?>
 <tr id="table_control">
  <td>
	<div id="footer">
	  <a href="#header"><img src="/images/up.png" title="������� � ������ ��������"></a>
	</div>
  </td>
  <td colspan="<?=$table_rowspan-1;?>">
   <a class="btn large primary btn_save" onclick="Button.Save(); return false" href="#">���������</a>
  <div id="checkbox_control" style="display: none">
    <select id="print_select">
    </select>
    <a class="btn success btn_print" href="#" onclick="Button.Print.Click(); return false">������</a>
    <a class="btn error" href="#" onclick="Button.SomeDelete(); return false">�������</a>
  </div>
  </td>
 </tr>
</table>



<code id="server_result">
</code>

</body>
</html>