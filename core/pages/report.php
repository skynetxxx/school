<?
$need[] = "CONFIG";
$need[] = "MYSQL";
include("config.php");
$max_file_size = ini_get("upload_max_filesize");
$user_id = $_SESSION["uid"];
$lists = GetAllLists($user_id);

function NoDir($dir) {
?>
<div class="alert-message error">
<p>����������� ������! ����� <b><?=$dir;?></b> �� ����������!</p>
<p>����������� ������ ��� ���� ����� ����������!</p>
</div><?
die();
}

function NoDoc() {?>
<div class="alert-message error">
<p>����������� ������! ����������� <b>xml</b> ����� ��� ������.</p>
<p>��������� ����� �������� ������ � ������� <b>xml</b>!</p>
</div>
<?}

function ShowFile($name,$link) {?>
<div class="myfile row">
  <div class="fname span3">
    <?=$name;?>
  </div>
  <div class="span2" style="margin-left: 2px;">
    <a class="btn success" href="/print/print/<?=$link;?>">������</a>
    <script>
      window.links.push("/print/print/<?=$link;?>");
    </script>
  </div>
  <div class="span4">
    <a class="btn info" href="/print/save/<?=$link;?>">�������</a>
    <!--<a class="btn info" href="/print/rename/<?=$link;?>">��������</a>-->
    <a class="btn error" href="/print/delete/<?=$link;?>" onclick="return confirm('�� �������, ��� ������ ������� ���� ������?');">X</a>
  </div>
</div>
<?
}


function CreateSelector() {	global $lists;
?><select onchange="ChangePrint(this);"><?
	foreach ($lists as $element) {		$name = $element["name"];
		$id = $element["lid"];
		?><option value="<?=$id;?>"><?=$name;?></option><?
	}
?></select><?}
?>
<!DOCTYPE html>
<html>
<head>
<title>�������� ���� �������</title>
<link rel="stylesheet" href="/core/bootstrap.min.css">
<script type="text/javascript" src="/core/jquery.js"></script>
<script type="text/javascript" src="/core/bootstrap-alerts.js"></script>
<style>
.myfile {	margin: 10px 15px;}

.fname {    background: none repeat scroll 0 0 #EEEEEE;
    border-radius: 3px 3px 3px 3px;
    line-height: 30px;
    min-height: 30px;
    text-align: center;
}
</style>
<script type="text/javascript">
$(document).ready(function(){	$("select").trigger("change"); //��������� ����� ������});

function ChangePrint(ithis) {	var select = $(ithis);
	var selected = $(ithis).find("option:selected").val()
	var myfile = $(".myfile").find("a.success")
	myfile.each(function(index){
	  var link = window.links[index];
	  $(this).attr("href",link+"/"+selected);
	});}

window.links=[];
</script>
</head>
<body>
<a class="alert-message warning" style="padding: 20px 5px 5px;" href="/" onclick="history.go(-1); return false"><img src="/images/back.png"/></a>
<div style="margin-top: 40px"></div>

<div style="">
<form>
  <div class="clearfix" style="margin-bottom: 30px">
	<div class="input" style="margin-bottom: 10px">
	  �������� ������ ��� ������:
	</div>
	<div class="input">
	  <?=CreateSelector();?>
	</div>
  </div>
</form>
</div>

<?
$lhasfile = false;

if (count($files)===0) {	NoDoc();} else {	foreach ($files as $num=>$file) {		ShowFile($file,$num);	}}
?>
<div style="margin-top: 90px;">
<form action="/upload/" method="post" enctype="multipart/form-data">
  <fieldset>
    <div class="clearfix">
     <div class="input" style="margin-bottom: 3px">
        <span style="margin-bottom: 4px">������������ ������ �����: <b><?=$max_file_size;?></b></span>
     </div>
      <div class="input">
        <input name="document" type="file" class="input-file">
     </div>
      <div class="input">
        <input type="submit" value="���������" class="btn primary small" style="margin-top: 10px; margin-left: 150px">
     </div>
    </div>
  </fieldset>
</form>
</div>

</body>
</html>