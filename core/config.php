<?
include_once("../config.php");
include_once("../modules/security.php");

$INIPATH = "../data/";
$dir = "../../docs/";

if ((isset($need)) AND (is_array($need))) {
  if (in_array("INI",$need)) {
    include("../modules/ini_fn.php");
  }
  if (in_array("CONFIG",$need)) {
    $docs = true;
    include("../../config.php");
  }
  if (in_array("MYSQL",$need)) {
    include("../modules/mysql.php");
  }
}
?>